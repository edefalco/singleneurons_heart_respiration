%This code repoduces the main ananlysis in Figure 4 of the article 'Single 
%neurons in thalamus and subthalamic nucleus process cardiac and respiratory 
%signals in humans' - Emanuela De Falco, Marco Solc� et al. 202X
%
%
%Author: Emanuela De Falco
%emanuela.defalco@epfl.ch
%�cole Polytechnique F�d�rale de Lausanne (EPFL)
%September 2022

% -------------------------
% Description of input data:
% - spk_times:       array of doubles (Nx1) containing the time of each detected 
%                    neural spike (in seconds, relative to the beginning of the recording) 
%                    with N being the total number of spikes detected for the given neuron
% - resp_timestamps: array of doubles (Tx1) containing the timestamps of the respiration signal 
%                    (in seconds, relative to the beginning of the recording)
% - resp_phase :     array of doubles (Tx1) containing the angular phase of the respiratory signal 
%                    (in rads, betwwen -pi and pi)
%                    epochs of time where the signal was too noisy are
%                    excluded and associated phase is NaN
%
%Example data to run this code is provided here in the Data/Example3 subfolder.
% --------------------------

%% Initial setup
% -->DOWNLOAD CircStat toolbox and add it to singleneurons_heart_respiration/subroutines folder
% -->CHANGE MATLAB PATH TO singleneurons_heart_respiration LOCAL DIRECTORY

%% set data and codes directories
clearvars
main_dir = pwd;
data_dir = [main_dir '/Data/Example3/'];
% add code directories to Matlab path
addpath (genpath([main_dir]));

%% load data
% load detected spike times for sorted unit (Wave_clus)
load([data_dir 'Spike_times.mat'],'spk_times') %times of detected spikes in s
%load Respiratory signal (extracted from the RR toolbox)
load([data_dir 'Respiration.mat'])

%% test modulation with surrogate test
[pval,mean_angle,spk_phases] = test_FR_Respiration_modulation (spk_times,resp_phase,resp_timestamps);

%convert mean angle from [-pi; pi] interval to [0 ; 2pi] interval if needed
if mean_angle<0; mean_angle = mean_angle +(2*pi); end 
%% plots
h= figure(); clf; h.WindowState = 'maximized'; %open figure
set(h,'DefaultLineLineWidth',1,'DefaultLineMarkerSize',8,'DefaultAxesLineWidth',1.5,'DefaultAxesFontSize',13,'DefaultAxesFontWeight','Bold');
colors= lines;

% ------ Circular histogram of spike density across repiratory phase
subplot(3,2,1);
h1 = circ_plot(spk_phases,'hist',[],14,true); 
r = circ_r(spk_phases);zm = r*exp(1i*mean_angle);
hold on; plot([0 real(zm)], [0, imag(zm)],'linewidth',2,'color','g') %add mean_angle direction      
title(sprintf([ 'Mean angle: '  num2str(mean_angle,'%.1f') ' rad. \n   pval surro = ' num2str(pval,'%1.1e') ]),'interpreter','none')
axis square

% ------ Raster plot in respiratory phase space
subplot(3,2,3);

%upsample respiratory phase to have a better phase resolution for plotting 
timestamp_diff = unique(round(diff(resp_timestamps),2)); % get 1/sr of timestamps
upsampled_timestamps = resp_timestamps(1):timestamp_diff/50:resp_timestamps(end)+(timestamp_diff*49/50); %upsample timestamps
upsampled_resp_phase = resample(resp_phase,50,1); %phase of the respiration signal
%find resp phase associated to each spike
[~,spk_bin] = histc(spk_times,upsampled_timestamps-(1/250));%find respiration timestamp for each spike
spk_phases = nan(size(spk_times)); %initialize array 
spk_phases(spk_bin~=0)= upsampled_resp_phase(spk_bin(spk_bin~=0)); %phase of each spike
spk_phases(spk_phases<0)=spk_phases(spk_phases<0)+(2*pi); %convert [-pi; pi] interval to [0 ; 2pi] interval 
%find peaks corresponding to the beginning of respiratory cycle
[peak_val, idx_peak ]= findpeaks(upsampled_resp_phase,'MinPeakDistance',500,'MinPeakHeight',3);
idx_peak(peak_val<3)=[]; %exclude local peaks that are too small

%raster plots in rad
spk_times_mat = nan(length(idx_peak),10000);
for ic=1:length(idx_peak) %loop on respiratory cycles
    %find beginning of next cycle
    if ic == length(idx_peak)%if last cycle 
        next_peak = idx_peak(ic)+ floor(median(diff(idx_peak)));
    else
        next_peak=idx_peak(ic+1);
    end
    %find spikes falling during the current cycle
    idx_spks = spk_bin >= idx_peak(ic)  & spk_bin< next_peak ;
    if isempty(idx_spks)
        tmp_spks=[];
    else
        tmp_spks = spk_phases(idx_spks);
    end
    spk_times_mat(ic,1:length(tmp_spks))= tmp_spks;
    scatter(tmp_spks,repmat(ic,1,length(tmp_spks)),4,repmat(colors(1,:),length(tmp_spks),1)); %plot 
    hold on
end
axis square tight
xlim([0 2*pi]);
hold on
plot([pi pi],ylim,'--r','linewidth',2)
plot([mean_angle mean_angle],ylim,'-g','linewidth',2)

set (gca,'Ydir','reverse');ylabel('Resp cycle #'); 
set(gca,'xtick',[0 pi 2*pi],'xticklabel',{'0','pi','2pi'}); xlabel('Respiration phase (rad)');

% ------ Histogram of spike count
subplot(3,2,5);

% bin spikes according to their times
[binned_times,spk_bin] = histc(spk_times,resp_timestamps);%find respiration timestamp for each spike
spk_phases= resp_phase(spk_bin(spk_bin~=0)); %associated resp phase for each spike
spk_phases(isnan(spk_phases))=[];
spk_phases(spk_phases<0)=spk_phases(spk_phases<0)+(2*pi); %convert [-pi; pi] interval to [0 ; 2pi] interval 

%calculate histogram in 10 bins
hspk = histc(spk_phases,[0:2*pi/10:2*pi]);
resp_phase(resp_phase<0)=resp_phase(resp_phase<0)+2*pi;

%plot
bar([0:2*pi/10:2*pi],hspk,'histc','HandleVisibility','off');
axis square; xlim([0 2*pi])
hold on
plot([0 pi],[1 1],'-r','linewidth',2) %mark expiration epoch
plot([pi 2*pi],[1 1],'-c','linewidth',2) %mark inspiration epoch
plot([mean_angle mean_angle],ylim,'-g','linewidth',2) %mark mean angle
set(gca,'xtick',[0 pi/2 pi pi*3/2 2*pi],'xticklabel',{'0' ,'', 'pi','','2pi'})
legend({'spike phase','expiration','inspiration'},'location','nw')
xlabel('Respiration phase (rad)'); ylabel('Spike count')


