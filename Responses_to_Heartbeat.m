%This code repoduces the main ananlysis in Figure 2 of the article 'Single 
%neurons in thalamus and subthalamic nucleus process cardiac and respiratory 
%signals in humans' - Emanuela De Falco, Marco Solc� et al. 202X
%
%
%Author: Emanuela De Falco
%emanuela.defalco@epfl.ch
%�cole Polytechnique F�d�rale de Lausanne (EPFL)
%September 2022

% -------------------------
% Description of input data:
% - spk_times:  array of doubles (Nx1) containing the time of each detected 
%               neural spike (in seconds, relative to the beginning of the recording) 
%               with N being the total number of spikes detected for the given neuron
% - Rpeaks:     array of doubles (Tx1) containing the times of the ECG R-peaks  
%                    (in seconds, relative to the beginning of the recording)
% - IBI :       array of doubles ((T-1)x1) containing the Interbeat interval (IBI, in sec) 
%               following each R-peak in Rpeaks 
%
%Example data to run this code is provided here in the Data/Example1 subfolder.
% --------------------------
%% Initial setup
% -->DOWNLOAD CircStat toolbox and add it to singleneurons_heart_respiration/subroutines folder
% -->CHANGE MATLAB PATH TO singleneurons_heart_respiration LOCAL DIRECTORY

%% set data and codes directories
clearvars
main_dir = pwd;
data_dir = [main_dir '/Data/Example1/'];
% add code directories to Matlab path
addpath (genpath([main_dir]))

%% load data
% load detected spike times for sorted unit (Wave_clus)
load([data_dir 'Spike_times.mat'],'spk_times') %times of detected spikes in s
%load Rpeaks from heplab results (in s)
load([data_dir 'Rpeaks.mat'],'Rpeaks','IBI')
%calculate median IBI
median_IBI = median(IBI);

%% Response to Heartbeat 

%test modulation with surrogate test
[pval] = test_FR_modulation (spk_times,Rpeaks);

%plots
h= figure(); clf; h.WindowState = 'maximized'; %open figure
set(h,'DefaultLineLineWidth',3,'DefaultLineMarkerSize',8,'DefaultAxesLineWidth',1.5,'DefaultAxesFontSize',13,'DefaultAxesFontWeight','Bold');
colors = lines; %define colors

%raster plots locked to Rpeak
subplot(2,3,[1])
spk_times_mat = nan(length(Rpeaks),10000);
for ir=1:length(Rpeaks)
    tmp_spks = spk_times(spk_times >= Rpeaks(ir)-0.1  & spk_times< Rpeaks(ir)+ median_IBI ) - Rpeaks(ir);
    spk_times_mat(ir,1:length(tmp_spks))= tmp_spks;
    scatter(tmp_spks,repmat(ir,1,length(tmp_spks)),4,repmat(colors(1,:),length(tmp_spks),1))
    hold on
end
xlim([-0.1 median_IBI])
set (gca,'Ydir','reverse'); ylabel('Cardiac cycle #');xlabel('time from Rpeak (s)')
title(sprintf(['Spike raster. \n Pval surrogate:' num2str(pval,'%1.1e')]))

%histogram of spike count
subplot(2,3,4)
x_bins = -0.1:0.02:median_IBI;
[counts,x_bins] = hist(spk_times_mat(:),x_bins);
hist(spk_times_mat(:),x_bins);
xlabel('time from Rpeak (s)')
ylabel('spikes count')
xlim([-0.1 median_IBI])
