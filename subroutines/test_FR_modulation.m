function [pval] = test_FR_modulation (spk_times,Rpeaks)
%Function to perform surrogate statistics on Modulation of spike activity
%relative to the Cardiac cycle

% -------INPUTS
% - spk_times:  array of doubles (Nx1) containing the time of each detected 
%               neural spike (in seconds, relative to the beginning of the recording) 
%               with N being the total number of spikes detected for the given neuron
% - Rpeaks:     array of doubles (Tx1) containing the times of the ECG R-peaks  
%               (in seconds, relative to the beginning of the recording)
%
% -------OUTPUTS
% - pval :     pvalue for the surrogate test for non-uniformity of spike 
%              activity across the cardiac cycle phase 

%% define surrogate parameters
nPerm = 1000; %number of random permutations
nJitter = 500; % jittering(ms)

%% caclulate median IBI and bins
medianIBI = nanmedian(diff(Rpeaks));
bin_step = medianIBI/10; bins= -0:bin_step:medianIBI;
phases=((2*pi)/(max(bins)))*(bins-max(bins))+2*pi; % calculate time bins as circular phases with Rpeak equal to 0rad and 2*pi rad

%% realign spike times to each Rpeak 
spk_times_mat = nan(length(Rpeaks),10000);%initialize aligned matrix
for ir=1:length(Rpeaks) %loop on Rpeaks
    tmp_spks = spk_times(spk_times >= Rpeaks(ir)-0.1  & spk_times< Rpeaks(ir)+ medianIBI)- Rpeaks(ir);
    spk_times_mat(ir,1:length(tmp_spks))= tmp_spks;   
end

%% bin spikes
[binned_times] = histc(spk_times_mat(~isnan(spk_times_mat)),bins);
%% calculate original z-statistic
[~, z] = circ_rtest(phases(2:end),binned_times(1:end-1)); 

%% surrogate statistic
perm=0; %permutation index 
z_perm = nan(nPerm,1); %inizialize surrogate statistic array
while perm<nPerm %loop on surrogates
    perm=perm+1;
    
    jitter = zeros(length(Rpeaks'),1);    
    for itrial = 1:length(Rpeaks)
        if mod(itrial,10) == 0 % jitter every 10 trial to preserve IBI and variability of cardiac cycles
            jitter(itrial-9:itrial) = (randperm(nJitter,1)-(nJitter/2))/1000;%in seconds 
        end
    end
    
    surro_Rpeaks  = Rpeaks' +jitter; %surrogate Rpeaks
    surro_medianIBI = median(diff(surro_Rpeaks)); %surrogate nmedian IBI
    surro_spk_times_mat = nan(length(surro_Rpeaks),10000);
    for ir=1:length(surro_Rpeaks)    
        tmp_spks = spk_times(spk_times >= surro_Rpeaks(ir)-0.1  & spk_times< surro_Rpeaks(ir)+ surro_medianIBI)- surro_Rpeaks(ir);
        surro_spk_times_mat(ir,1:length(tmp_spks))= tmp_spks;
    end
    bin_step = surro_medianIBI/10;
    bins= 0:bin_step:surro_medianIBI;
    phases=((2*pi)/(max(bins)))*(bins-max(bins))+2*pi; % calculate time bins as circular phases with Rpeak equal to 0rad and 2*pi rad
    [binned_times] = histc(surro_spk_times_mat(~isnan(surro_spk_times_mat)),bins);
    [~, z_perm(perm)] = circ_rtest(phases(2:end),binned_times(1:end-1)); %phase in rad
end

%% calculate pvalue
pval = (sum(z_perm>=z)+1)/sum(~isnan(z_perm));

end