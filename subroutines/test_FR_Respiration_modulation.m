function [pval,mean_angle,spk_phases] = test_FR_Respiration_modulation(spk_times,resp_phase,resp_timestamps)
%function to perform surrogate statistics on Modulation of spike activity
%relative to Respiratory phase

% -------INPUTS
% - spk_times:       array of doubles (Nx1) containing the time of each detected 
%                    neural spike (in seconds, relative to the beginning of the recording) 
%                    with N being the total number of spikes detected for the given neuron
% - resp_timestamps: array of doubles (Tx1) containing the timestamps of the respiration signal 
%                    (in seconds, relative to the beginning of the recording)
% - resp_phase :     array of doubles (Tx1) containing the angular phase of the respiratory signal 
%                    (in rads, betwwen -pi and pi)
%                    epochs of time where the signal was too noisy are
%                    excluded and associated phase is NaN
%
% -------OUTPUTS
% - pval       : pvalue for the surrogate test for non-uniformity of spike 
%                activity across respiratory phase 
% - mean_angle : mean direction (in rad) of respiratory phases associated to the spikes
% - spk_phase  : array of doubles (Nx1) containing the Respiratory phase associated 
%                to each spike in in spk_times (in rads, betwwen -pi and pi)

%% set parameters
Nperm = 10000; % number of surrogates to generate

%% Calculate original z-statistic

%extract respiratory phase ssociated to each spike
[~,spk_bin] = histc(spk_times,resp_timestamps);%find respiration timestamp for each spike
spk_phases= resp_phase(spk_bin(spk_bin~=0)); %phase of each spike
spk_phases(isnan(spk_phases))=[];
%calculate mean angle
mean_angle = circ_mean([spk_phases]);

%calculate original z value Rayleigh test
[~, zspk] = circ_rtest([spk_phases]);

%% Create surrogate population 
%initialize variables
perm=0; z_perm = nan(10000,1);
while perm<Nperm
    perm=perm+1;
    jit_spikes = spk_times+ ((rand(length(spk_times),1)-0.5)*5); %jitter all spikes by a random time between -2.5s and +2.5s
    
    [~,spk_bin] = histc(jit_spikes,resp_timestamps);%find respiration timestamp for each spike
    jit_spk_phases= resp_phase(spk_bin(spk_bin~=0)); %phase of each spike
    jit_spk_phases(isnan(jit_spk_phases))=[];
    
    [~, z_perm(perm)] = circ_rtest(jit_spk_phases); %phase in rad
end
%% calculate pvalue surrogate test
pval = (sum(z_perm>=zspk)+1)/sum(~isnan(z_perm));

