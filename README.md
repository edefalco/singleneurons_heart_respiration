# SingleNeurons_Heart_Respiration
This repository include codes to repoduce the main analysis from the article :
'Single neurons in thalamus and subthalamic nucleus process cardiac and respiratory signals in humans' - Emanuela De Falco, Marco Solcà et al. 202X

Author: Emanuela De Falco (emanuela.defalco@epfl.ch), École Polytechnique Fédérale de Lausanne (EPFL),September 2022

## Software dependencies
All analysis codes are written in Matlab and have been developed and tested with Matlab R2019b
To install/ use Matlab see: https://www.mathworks.com/products/new_products/release2019b.html (expected time 30 minutes)

The codes uses the CircStat toolbox (P. Berens, CircStat: A Matlab Toolbox for Circular Statistics, Journal of Statistical Software, Volume 31, Issue 10, 2009 http://www.jstatsoft.org/v31/i10). 
The toolbox can be found here: https://github.com/circstat/circstat-matlab . 
Once downloaded, add the unzipped circstat-matlab-master folder to your local directory singleneurons_heart_respiration/subroutines/

## Usage
Three main scripts are provided to reproduce the main custom analysis and plots in the article:

- Responses_to_Heartbeat.m         : reproduces the custom analysis/main plot in Figure 2                                   
- Correlation_FR_IBI.m             : reproduces the custom analysis/main plot in Figure 3
- Responses_to_Respiratory_cycle.m : reproduces the custom analysis/main plot in Figure 4

In order to run each script in Matlab:
- CHANGE MATLAB PATH TO singleneurons_heart_respiration LOCAL DIRECTORY
- Launch the script from Matlab command window by typing it's name and pressing Enter 
  (Expected runtime for each script is less than one minute)

Example data to run each script is provided here in the /Data subfolder of the repository.

Scripts can be tested on different data by providing appropriate inputs. 
Here a list of the input data required by the repo. More details can be found in each script:
 - spk_times:       array of doubles (Nx1) containing the time of each detected 
                    neural spike (in seconds, relative to the beginning of the recording) 
                    with N being the total number of spikes detected for the given neuron
 - Rpeaks:          array of doubles (Tx1) containing the times of the ECG R-peaks  
                    (in seconds, relative to the beginning of the recording)
 - IBI :            array of doubles ((T-1)x1) containing the Interbeat interval (IBI, in sec) 
                    following each R-peak in Rpeaks 
 - resp_timestamps: array of doubles (Tx1) containing the timestamps of the respiration signal 
                    (in seconds, relative to the beginning of the recording)
 - resp_phase :     array of doubles (Tx1) containing the angular phase of the respiratory signal 
                    (in rads, betwwen -pi and pi)
                    epochs of time where the signal was too noisy are
                    excluded and associated phase is NaN

## Reference
De Falco, E.; Solcà, M. et al. 202X - 'Single neurons in thalamus and subthalamic nucleus process cardiac and respiratory signals in humans' 

Please cite this research article when the provided code is used. 

## License
Copyright [2022] [Emanuela De Falco]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
