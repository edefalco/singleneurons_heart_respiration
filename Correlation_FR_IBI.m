%This code repoduces the main ananlysis in Figure 3 of the article 'Single
%neurons in thalamus and subthalamic nucleus process cardiac and respiratory
%signals in humans' - Emanuela De Falco, Marco Solc� et al. 202X
%
%
%Author: Emanuela De Falco
%emanuela.defalco@epfl.ch
%�cole Polytechnique F�d�rale de Lausanne (EPFL)
%September 2022

% -------------------------
% Description of input data:
% - spk_times:  array of doubles (Nx1) containing the time of each detected
%               neural spike (in seconds, relative to the beginning of the recording)
%               with N being the total number of spikes detected for the given neuron
% - Rpeaks:     array of doubles (Tx1) containing the times of the ECG R-peaks
%                    (in seconds, relative to the beginning of the recording)
% - IBI :       array of doubles ((T-1)x1) containing the Interbeat interval (IBI, in sec)
%               following each R-peak in Rpeaks
%
%Example data to run this code is provided here in the Data/Example2 subfolder.
% --------------------------

%% Initial setup
% -->DOWNLOAD CircStat toolbox and add it to singleneurons_heart_respiration/subroutines folder
% -->CHANGE MATLAB PATH TO singleneurons_heart_respiration LOCAL DIRECTORY

%% set data and codes directories
clearvars
main_dir = pwd;
data_dir = [main_dir '/Data/Example2/'];
% add code directories to Matlab path
addpath (genpath([main_dir]))

%% load data
% load detected spike times for sorted unit (Wave_clus)
load([data_dir 'Spike_times.mat'],'spk_times') %times of detected spikes in s
%load Rpeaks from heplab results (in s)
load([data_dir 'Rpeaks.mat'],'Rpeaks','IBI')
%calculate median IBI
median_IBI = median(IBI);

%% calculate firing rates per cardiac cycle
neural_fr = nan(length(Rpeaks)-1,1);
for i = 1:length(Rpeaks)-1 %count normalized firing rate (num spikes fired divided by cardiac cycle duration) for each cardiac cycle
    neural_fr(i) = sum(spk_times>= Rpeaks(i) & spk_times<Rpeaks(i)+IBI(i))/IBI(i); %firing rate
end

%% Plot correlations and raster plots
h= figure(); clf; h.WindowState = 'maximized'; %open figure
set(h,'DefaultLineLineWidth',3,'DefaultLineMarkerSize',8,'DefaultAxesLineWidth',1.5,'DefaultAxesFontSize',13,'DefaultAxesFontWeight','Bold');

%calculate correlation coeffient and pvalue (Pearson)
IBI = IBI'; %transpose array
indnan=isnan(IBI) | isnan(neural_fr); %index to identify eventual NaNs in arrays
[r,pval]=corr(IBI(~indnan),neural_fr(~indnan), 'type' ,'Pearson');

%plot correlation
subplot(2,2,1)
scatter(IBI(~indnan),neural_fr(~indnan),50);
hls = lsline; set(hls,'Tag','lsLines'); %add least square fit line
ylabel('FR (Hz)','interpreter','none'); xlabel('IBI (s)','interpreter','none'); axis tight;
title(sprintf(['Correlation FR-IBI. \n R= %.2f, pval= %1.1e ' ],r,pval))

% raster plot sorted by CCD duration
subplot(2,2,3)
%compute matrix of spike times aligned to Rpeaks
spk_times_mat = nan(length(Rpeaks)-1,10000);
for ir=1:length(Rpeaks)-1
    tmp_spks = spk_times(spk_times >= Rpeaks(ir)-0.1  & spk_times< Rpeaks(ir)+ IBI(ir) ) - Rpeaks(ir);
    spk_times_mat(ir,1:length(tmp_spks))= tmp_spks;
end
[~,sort_ind] = sort(IBI); %index to sort IBIs based on duration
for ii=1:length(Rpeaks)-1
    tmp_spks = spk_times_mat(ii,~isnan(spk_times_mat(ii,:)));
    scatter(tmp_spks,repmat(find(sort_ind==ii),1,length(tmp_spks)),2,'.','b')
    hold on
end
% add markers to indicate end of cardiac cycle in green
scatter(IBI(sort_ind),1:length(IBI),'g.')

set (gca,'Ydir','reverse'); ylabel('sorted Cardiac cycle #'); xlabel('time from Rpeak (s)'); axis tight;